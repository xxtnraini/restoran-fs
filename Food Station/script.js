const makanan = [ 
            {
              gambar : "image/menu 4.jpg",
              judul : "Simple Set Teriyaki 2",
              harga : 40000 
            },
            {
              gambar : "image/menu/ck.jpg",
              judul : "Chicken Karaage",
              harga : 35000 
            },
            {
              gambar : "image/menu/s2'2'.jpg",
              judul : "TORI FINGER 10 PCS",
              harga : 35000 
            }, 
            {   
              gambar : "image/menu/s1'1'.jpg",
              judul : "Hot Chicken Steak",
              harga : 30000 
            },
            {
              gambar : "image/menu/bfyk.jpg",
              judul : "Beef Yakiniku",
              harga : 40000 
            },
            {
              gambar : "image/menu/s1'3'.jpg",
              judul : "Hot Spicy Beef Teriyaki",
              harga : 40000 
            }, 
            {
              gambar : "image/menu/s1.jpg",
              judul : "Paket Hemat 1",
              harga : 25000 
            },
            {
              gambar : "image/menu/s2.jpg",
              judul : "Paket Hemat 2",
              harga : 25000 
            },
            {
              gambar : "image/menu/s3.jpg",
              judul : "Paket Hemat 3",
              harga : 25000  
            },
            {
              gambar : "image/menu/d1.jpg",
              judul : "Puding Cokelat",
              harga : 25000 
            },
            {
              gambar : "image/menu/d2.jpg",
              judul : "Soft Taro Puding",
              harga : 15000 
            },
            {
              gambar : "image/menu/d3.jpg",
              judul : "Es Ogura",
              harga : 30000 
            } 

      ];

const jumlahMenu = (array) => {
  const jmlItemUnsur = document.querySelector('.kolom-pesan h3');
  const jumlahItem = array.reduce((accumulator) => {
    return accumulator + 1;
  }, 0);
  jmlItemUnsur.innerHTML = jumlahItem;
}
  

const callbackMap = (item, index, array)=>{
 const elmnt = document.querySelector(".makanan-menu");

  elmnt.innerHTML +=  ` 
      <div class="container" style="margin-right:1px;">
        <div class="row" style="float: left;">
          <div class="col-sm m-2" >
            <div class="card m-3" style="width: 18rem;">
              <img src="${item.gambar}" class="card-img-top" alt="...">
              <div class="card-body bg-light">
                <h5 class="card-title">${item.judul}</h5>=
                <p><b>Rp.${item.harga}</b></p><br>
                <p class="card-text">${item.deskripsi}</p>
                <i class="fas fa-star text-success"></i>
                <i class="fas fa-star text-success"></i>
                <i class="fas fa-star text-success"></i>
                <i class="fas fa-star text-success"></i><br>
                <a href="#" class="btn btn-primary" data-target="#menu1" data-toggle="modal">Detail</a>
                <a class="btn btn-danger">Beli Sekarang</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    `
}
   
makanan.map(callbackMap);
  jumlahMenu(makanan);


  const buttonElement = document.querySelector('.button-keyword');

  buttonElement.addEventListener('click',()=>{
    const hasilPencarian = makanan.filter((item, index)=>{
                              const inputElemnt = document.querySelector('.input-keyword');
                              const namaItem    = item.judul.toLowerCase();
                              const keyword     = inputElemnt.value.toLowerCase();


                              return namaItem.includes(keyword);
                        })

      document.querySelector('.makanan-menu').innerHTML = '';

      hasilPencarian.map(callbackMap);
      jumlahMenu(hasilPencarian);

  });
  

